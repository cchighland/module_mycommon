<?php

/**
 * Defines the global footer block.
 */
class MyCommonFooterBlock extends MyCommonBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Common: Footer message.'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $home = l(
      'Cornerstone Church of Highland &bull; 4995 N Hickory Ridge Rd &bull; Highland, MI 48357 &bull; 248-887-1600',
      'https://www.cornerstonehighland.com/',
      [
        'attributes' => ['class' => 'cc-home-link', 'title' => t('Welcome to Cornerstone. Welcome Home.')],
        'html' => TRUE,
      ]
    );
    $output[] = ['#markup' => t('Copyright &copy;!year !home All rights reserved', ['!year' => date('Y'), '!home' => $home])];
  }
}
